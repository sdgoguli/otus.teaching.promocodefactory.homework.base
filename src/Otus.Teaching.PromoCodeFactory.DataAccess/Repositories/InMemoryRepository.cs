﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> DeleteEntityAsync(Guid id)
        {
            try
            {
                var filteredData = Data.Where(x => x.Id != id);
                Data = filteredData;
                return Task.FromResult(id);
            }
            catch // проглатывать не хорошо, но мы пока не дошли до логгирования
            {
                return Task.FromResult(Guid.Empty);
            }
        }

        public Task<T> AddNewEntityAsync(T entity)
        {
            try
            {
                var temp = Data.Append(entity);
                Data = temp;
                return Task.FromResult(entity);
            }
            catch // проглатывать не хорошо, но мы пока не дошли до логгирования
            {                
                return null;
            }
        }

        public Task<T> UpdateByIdAsync(Guid id, T entity)
        {
            try
            {
                var entityToUpdate = Data.FirstOrDefault(x => x.Id == id);
                entityToUpdate = entity;
                return Task.FromResult(entityToUpdate);
            }
            catch // проглатывать не хорошо, но мы пока не дошли до логгирования
            {
                return null;
            }
        }
    }
}