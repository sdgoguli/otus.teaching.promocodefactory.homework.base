﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;
        public EmployeesController(IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeCreateResponse>> AddNewEmployeeAsync([FromBody] EmployeeCreateRequest request)
        {
            if (request == null)
                return BadRequest(new EmployeeCreateResponse 
                {
                    IsSuccess = false,
                    ErrorMessage = "Все входные параметры пусты"  
                });
            
            var allRoles = await _roleRepository.GetAllAsync();
            var chosenRoles = allRoles.Where(r => request.RoleNames.Any(n => n == r.Name)).ToList();

            if (chosenRoles.Count <= 0)
                return BadRequest(new EmployeeCreateResponse
                {
                    IsSuccess = false,
                    ErrorMessage = "Не нашли ни одной указанной роли",
                    MissingRoleNames = request.RoleNames
                });

            var missingRoles = request.RoleNames.Where(r => !chosenRoles.Any(cr => cr.Name == r)).ToList();

            if (missingRoles.Count > 0)
                return BadRequest(new EmployeeCreateResponse
                {
                    IsSuccess = false,
                    ErrorMessage = "Не нашли следующие указанные роли",
                    MissingRoleNames = missingRoles
                });

            var employee = new Employee
            {                
                Id = Guid.NewGuid(),
                AppliedPromocodesCount = request.AppliedPromocodesCount,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Roles = chosenRoles
            };

            var result = await _employeeRepository.AddNewEntityAsync(employee);

            if (result == null)
                return BadRequest(new EmployeeCreateResponse
                {
                    IsSuccess = false,
                    ErrorMessage = "Неудачная попытка добавить сущность"
                });

            return Ok(new EmployeeCreateResponse { IsSuccess = true });
        }

        /// <summary>
        /// Удалить сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> RemoveEmployeeByIdAsync(Guid id)
        {
            var guid = await _employeeRepository.DeleteEntityAsync(id);

            if (guid == Guid.Empty)
                return NotFound();            

            return Ok();
        }

        ///// <summary>
        ///// Обновить данные о сотруднике по id
        ///// </summary>
        ///// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<BaseResponse>> UpdateEmployeeByIdAsync([FromBody]EmployeeUpdateRequest request)
        {
            if (request == null)
                return BadRequest(new BaseResponse
                {
                    IsSuccess = false,
                    ErrorMessage = "Все входные параметры пусты"
                });

            var employee = await _employeeRepository.GetByIdAsync(request.Id);

            if (employee == null)
                return NotFound(new BaseResponse
                {
                    IsSuccess = false,
                    ErrorMessage = "Не нашли сотрудника для изменения"
                });

            employee.AppliedPromocodesCount = request.AppliedPromocodesCount ?? employee.AppliedPromocodesCount;
            employee.FirstName = string.IsNullOrWhiteSpace(request.FirstName) ? employee.FirstName : request.FirstName;
            employee.LastName = string.IsNullOrWhiteSpace(request.LastName) ? employee.LastName : request.LastName;
            employee.Email = string.IsNullOrWhiteSpace(request.Email) ? employee.Email : request.Email;

            var updatedEmployee = await _employeeRepository.UpdateByIdAsync(request.Id, employee);

            if (updatedEmployee == null)
                return BadRequest(new BaseResponse
                {
                    IsSuccess = false,
                    ErrorMessage = "Неудачная попытка обновить сущность"
                });

            return Ok(new BaseResponse { IsSuccess = true });
        }
    }
}