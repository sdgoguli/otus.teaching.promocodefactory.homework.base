﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeCreateResponse : BaseResponse
    {
        public ICollection<string> MissingRoleNames { get; set; }
    }
}
